#pragma once

#include "Grenade.hpp"

class HolyHandGrenade : public Grenade {
  public:
    virtual const char * GetName();
    virtual const char * GetDisplayName();
    virtual const char * GetItemTypeName();
    virtual const char * GetDescription();
    virtual const char * GetFlavorText();
    virtual float GetCooldownTime();
    virtual int32_t GetDamage();
    virtual enum DamageType GetDamageType();
    virtual enum ItemRarity GetItemRarity();
    virtual int32_t GetTradeValue();
    virtual void SpawnProjectile(class Player *);
};