#pragma once

#include <stddef.h>
#include <cstdint>

struct ItemCountInfo {
    uint32_t count;
    uint32_t loadedAmmo;
};