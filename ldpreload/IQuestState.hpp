#pragma once

class IQuestState {
  public:
    virtual const char * GetName();
    virtual const char * GetDescription();
    virtual void CheckForEarlyCompletion(class IPlayer *);
    virtual void OnItemAcquired(class IPlayer *, class IItem *);
    virtual void OnItemPickupUsed(class IPlayer *, const char *);
};