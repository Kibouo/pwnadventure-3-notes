#pragma once

#include "Spawner.hpp"

class GiantRatSpawner : public Spawner {
  public:
    GiantRatSpawner(const Vector3 &, const Rotation &);
    virtual Actor * Spawn(void);
};