#pragma once

#include "IQuestState.hpp"
#include <string>

class QuestState : public IQuestState {
  protected:
    class Quest *m_quest;
    std::string m_name;
    std::string m_description;

    void AdvanceToState(class IPlayer *, const std::string &);
  public:
    QuestState(class Quest *, const std::string &, const std::string &);
    virtual const char * GetName();
    virtual const char * GetDescription();
    virtual void CheckForEarlyCompletion(class IPlayer *);
    virtual void OnItemAcquired(class IPlayer *, class IItem *);
    virtual void OnItemPickupUsed(class IPlayer *, const char *);
};