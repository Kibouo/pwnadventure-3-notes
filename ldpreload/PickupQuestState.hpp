#pragma once

#include "QuestState.hpp"

class PickupQuestState : public QuestState {
  private:
    std::string m_pickupName;
    std::string m_nextState;

  public:
    PickupQuestState(class Quest *, const std::string &, const std::string &, const std::string &, const std::string &);
    virtual void CheckForEarlyCompletion(class IPlayer *);
    virtual void OnItemPickupUsed(class IPlayer *, const char *);
};