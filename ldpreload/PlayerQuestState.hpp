#pragma once

#include <stddef.h>
#include <cstdint>

struct PlayerQuestState {
    class IQuestState *state;
    uint32_t count;
};