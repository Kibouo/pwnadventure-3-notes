#pragma once

#include "Flag.hpp"

class FlagOfTheBlock : public Flag {
  public:
    virtual const char * GetName();
    virtual const char * GetDisplayName();
};