#pragma once

#include "imgui/imgui.h"
#include "imgui/backends/imgui_impl_glfw.h"
#include "imgui/backends/imgui_impl_opengl3.h"
#include <GL/glew.h>
#include "Player.hpp"

#include <GLFW/glfw3.h>

struct GuiState {
    bool locked_hp = false;
    bool locked_mp = false;
    int walking_speed;
    int jump_speed;

    Vector3 a = {};

    GuiState(Player * player){
        this->walking_speed = player->GetWalkingSpeed();
        this->jump_speed = player->GetJumpSpeed();
    }
};

void glfw_error_callback(int error, const char* description);
GLFWwindow*  gui_init();
void gui_main_loop_inner(GLFWwindow* window, Player* player, GuiState *gui_state);
