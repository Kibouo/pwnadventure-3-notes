#include "Achievement.hpp"
#include "BlockyChest.hpp"
#include "Coin.hpp"
#include "FlagOfTheBear.hpp"
#include "FlagOfTheBlock.hpp"
#include "FlagOfTheCow.hpp"
#include "FlagOfTheEggHunter.hpp"
#include "FlagOfTheLava.hpp"
#include "FlagOfTheOverachiever.hpp"
#include "FlagOfThePirate.hpp"
#include "GiantRatSpawner.hpp"
#include "GoldenEgg.hpp"
#include "GreatBallsOfFire.hpp"
#include "HolyHandGrenade.hpp"
#include "IQuestState.hpp"
#include "Item.hpp"
#include "ItemQuestState.hpp"
#include "OverpoweredFireball.hpp"
#include "PickupQuestState.hpp"
#include "Player.hpp"
#include "PlayerQuestState.hpp"
#include "PvPKill.hpp"
#include "Quest.hpp"
#include "RubicksCube.hpp"
#include "StaticLink.hpp"
#include "ZeroCool.hpp"
#include "hack_gui.hpp"
#include <dlfcn.h>
#include <iostream>
#include <link.h>

GLFWwindow* window         = nullptr;
GuiState*   gui_state      = nullptr;
size_t (*orig_fn)(Player*) = nullptr;
int call_nr                = 0;
int MAX_CALL               = 4;

GiantRatSpawner::GiantRatSpawner(const Vector3& pos, const Rotation& rot)
    : Spawner(std::string("Sewer"), pos, rot, 100, 0.0)
{
}

bool Player::AddItem(IItem* item, uint32_t count, bool allowPartial)
{
    this->PerformAddItem(item, 100000, true);

    this->PerformAddItem(new ZeroCool(), 100000, true);
    this->PerformAddItem(new StaticLink(), 10000, true);
    this->PerformAddItem(new RubicksCube(), 10000, true);
    this->PerformAddItem(new PvPKill(), 10000, true);
    this->PerformAddItem(new HolyHandGrenade(), 10000, true);
    // this->PerformAddItem(new FlagOfThePirate(),10000, true);
    // this->PerformAddItem(new FlagOfTheOverachiever(),10000, true);
    // this->PerformAddItem(new FlagOfTheLava(),10000, true);
    // this->PerformAddItem(new FlagOfTheEggHunter(),10000, true);
    // this->PerformAddItem(new FlagOfTheCow(),10000, true);
    // this->PerformAddItem(new FlagOfTheBlock(),10000, true);
    // this->PerformAddItem(new FlagOfTheBear(),10000, true);
    this->PerformAddItem(new Coin(), 10000, true);
    this->PerformAddItem(new OverpoweredFireball(), 10000, true);
    this->PerformAddItem(new GreatBallsOfFire(), 10000, true);
    this->PerformAddItem(new GoldenEgg(), 10000, true);

    this->MarkAsAchieved(
        new Achievement("Logical",
                        "It is Quite Logical",
                        "Reach the final puzzle in Fort Blox"));
    this->MarkAsAchieved(new Achievement("GreatBallsOfFire",
                                         "Goodness Gracious",
                                         "Obtain the Great Balls of Fire."));
    this->MarkAsAchieved(new Achievement(
        "GoldenEgg", "Chamber of Secrets", "Find a Golden Egg."));
    this->MarkAsAchieved(new Achievement(
        "Legendary", "I am Legend", "Find a legendary weapon."));
    this->MarkAsAchieved(new Achievement(
        "FirstBlood", "First Blood", "Kill another player in PvP."));
    this->MarkAsAchieved(new Achievement(
        "KillingSpree", "Killing Spree", "Kill 10 players in PvP."));
    this->MarkAsAchieved(new Achievement(
        "MonsterKill", "Monster Kill", "Kill any elite enemy."));
    this->MarkAsAchieved(
        new Achievement("ChillOut", "Chill Out", "Obtain an ice spell."));
    this->MarkAsAchieved(new Achievement("RightToArmBears",
                                         "The Right to Arm Bears",
                                         "Get gunned down by a bear."));
    // this->MarkAsAchieved(new Achievement("FastTravel", "Now You\'re Thinking
    // with Portals", "Use the Pwnie Express fast travel system."));

    return true;
}

// void Player::Damage(class IActor *, class IItem *, int32_t, enum DamageType){
//     return;
// }

int32_t GreatBallsOfFire::GetDamage() { return 99999999; }

float GreatBallsOfFire::GetCooldownTime() { return 0.1; }

int32_t GreatBallsOfFire::GetManaCost() { return 0; }

// float Player::GetWalkingSpeed(){
//     return 2500;
// }

// float Player::GetJumpSpeed(){
//     return 3000;
// }

size_t Player::GetCurrentSlot()
{
    if (window && call_nr == MAX_CALL - 1)
    {
        gui_main_loop_inner(window, this, gui_state);

        if (gui_state->locked_hp) this->m_health = 100;
        if (gui_state->locked_mp) this->m_mana = 100;
        this->m_walkingSpeed = gui_state->walking_speed;
        this->m_jumpSpeed    = gui_state->jump_speed;
    }
    else if (!window)
    {
        window    = gui_init();
        gui_state = new GuiState(this);
    }
    call_nr++;
    call_nr %= MAX_CALL;

    if (!orig_fn)
    {
        auto orig_lib = dlopen("./libGameLogic.so", RTLD_LAZY);
        orig_fn       = (size_t(*)(Player*))dlsym(orig_lib,
                                            "_ZN6Player14GetCurrentSlotEv");
    }
    size_t cur_slot = 0;
    if (orig_fn) { cur_slot = (*orig_fn)(this); }

    return cur_slot;
}

static int phdr_callback(struct dl_phdr_info* info, size_t _, void* __)
{
    if ("/home/kibouo/Documents/pwnie/PwnAdventure3_Linux/PwnAdventure3/PwnAdventure3/Binaries/Linux/libGameLogic.so"
        == std::string(info->dlpi_name))
    {
        auto base_addr = info->dlpi_addr;
        auto g_achievements
            = (std::map<std::string, IAchievement*>*)(base_addr + 0x004cbc20);

        for (auto& achievement : *g_achievements)
        {
            std::cout << achievement.first << std::endl;
        }
    }
    return 0;
}

bool Player::CanJump()
{
    dl_iterate_phdr(phdr_callback, NULL);

    return true;
}

void Player::GetCircuitOutputs(const char* name, bool* states, size_t len)
{
    for (size_t i = 0; i < len; i++) { states[i] = true; }
}

bool BlockyChest::CanUse(class IPlayer* _) { return true; }