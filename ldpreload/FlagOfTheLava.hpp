#pragma once

#include "Flag.hpp"

class FlagOfTheLava : public Flag {
  public:
    virtual const char * GetName();
    virtual const char * GetDisplayName();
};