#pragma once

#include "Flag.hpp"

class FlagOfThePirate : public Flag {
  public:
    virtual const char * GetName();
    virtual const char * GetDisplayName();
};