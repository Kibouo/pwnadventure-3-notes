#pragma once

#include "IQuest.hpp"
#include <string>
#include <map>

class Quest : public IQuest {
  protected:
    std::string m_name;
    std::string m_description;
    class std::map<std::basic_string<char>, IQuestState*, std::less<std::basic_string<char> >, std::allocator<std::pair<std::basic_string<char> const, IQuestState*> > > m_states;
    class IQuestState *m_startState;

  public:
    Quest(const std::string &, const std::string &);
    void AddState(class IQuestState *);
    void AddStartingState(class IQuestState *);
    virtual const char * GetName();
    virtual const char * GetDescription();
    virtual class IQuestState * GetStartingState();
    virtual class IQuestState * GetStateByName(const char *);
};