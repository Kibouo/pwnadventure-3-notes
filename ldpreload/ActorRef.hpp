#pragma once

template <typename T>
struct ActorRef {
    T * m_object;
};