#pragma once

#include <stddef.h>
#include <cstdint>

struct ItemAndCount {
    class IItem *item;
    uint32_t count;
    uint32_t loadedAmmo;
};