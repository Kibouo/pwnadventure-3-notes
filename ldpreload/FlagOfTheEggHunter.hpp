#pragma once

#include "Flag.hpp"

class FlagOfTheEggHunter : public Flag {
  public:
    virtual const char * GetName();
    virtual const char * GetDisplayName();
};