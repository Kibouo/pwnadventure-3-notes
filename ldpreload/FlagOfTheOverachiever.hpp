#pragma once

#include "Flag.hpp"

class FlagOfTheOverachiever : public Flag {
  public:
    virtual const char * GetName();
    virtual const char * GetDisplayName();
};