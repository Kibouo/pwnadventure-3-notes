#pragma once

#include "Flag.hpp"

class FlagOfTheBear : public Flag {
  public:
    virtual const char * GetName();
    virtual const char * GetDisplayName();
};