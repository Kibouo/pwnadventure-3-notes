#pragma once

#include "IAchievement.hpp"
#include <string>

class Achievement : public IAchievement
{
    private:
    std::string m_name;
    std::string m_displayName;
    std::string m_description;

    public:
    Achievement(const std::string&, const std::string&, const std::string&);
    virtual const char* GetName();
    virtual const char* GetDisplayName();
    virtual const char* GetDescription();
};