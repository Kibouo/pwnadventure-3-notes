#include <iostream>
#include "hack_gui.hpp"

// taken and modified from: https://github.com/ocornut/imgui/blob/master/examples/example_glfw_opengl3/main.cpp

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

void glfw_error_callback(int error, const char* description){
    std::cerr <<"Glfw Error " << error << ": " << description << std::endl;
}

GLFWwindow* gui_init(){
    // opengl/glfw init
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit()){
        std::cerr << "Failed to init GLFW" << std::endl;
        return nullptr;
    }

    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    GLFWwindow* window = glfwCreateWindow(800, 200, "player stats", NULL, NULL);
    if (window == NULL){
        std::cerr << "Failed to init window" << std::endl;
        return nullptr;
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    bool err = glewInit() != GLEW_OK;
    if (err)
    {
        std::cerr << "Failed to initialize OpenGL loader!" << std::endl;
        return nullptr;
    }

    // imgui init
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    return window;
}

void gui_main_loop_inner(GLFWwindow* window, Player* player, GuiState *gui_state){
    if (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        {
            ImGui::Begin("player stats");

            ImGui::Text("HP: ");
            ImGui::SameLine();
            ImGui::Value("", player->GetHealth());
            ImGui::SameLine();
            ImGui::Checkbox("##HP", &gui_state->locked_hp);

            ImGui::Text("MP: ");
            ImGui::SameLine();
            ImGui::Value("", player->GetMana());
            ImGui::SameLine();
            ImGui::Checkbox("##MP", &gui_state->locked_mp);

            ImGui::Text("Walk speed: ");
            ImGui::SameLine();
            ImGui::Value("", player->GetWalkingSpeed());
            ImGui::SameLine();
            ImGui::SliderInt("##Walk", &gui_state->walking_speed, 0, 10000);

            ImGui::Text("Jump height: ");
            ImGui::SameLine();
            ImGui::Value("", player->GetJumpSpeed());
            ImGui::SameLine();
            ImGui::SliderInt("##Jump", &gui_state->jump_speed, 0, 10000);

            ImGui::End();
        }

        {
            ImGui::Begin("Teleportr", nullptr, ImGuiWindowFlags_MenuBar);
            if (ImGui::BeginMenuBar())
            {
                if (ImGui::BeginMenu("Teleport to..."))
                {
                    if (ImGui::MenuItem("Custom"))
                        player->SetPosition(gui_state->a);

                    if (ImGui::MenuItem("Town"))
                        player->SetPosition(
                            Vector3(-39500.0, -20100.0, 2519.0));

                    if (ImGui::MenuItem("Ballmer Peak"))
                        player->SetPosition(
                            Vector3(-6705.0, -11680.0, 10527.0));

                    if (ImGui::MenuItem("Unbearable Woods"))
                        player->SetPosition(Vector3(-15860.0, 35250.0, 1219.0));

                    if (ImGui::MenuItem("The Pirate Bay"))
                        player->SetPosition(Vector3(45783.0, 58671.0, 533.0));

                    if (ImGui::MenuItem("Tail Mountains"))
                        player->SetPosition(Vector3(42981.0, 5676.0, 426.0));

                    if (ImGui::MenuItem("Cowbungalow"))
                        player->SetPosition(
                            Vector3(260115.0, -248743.0, 1514.0));

                    if (ImGui::MenuItem("Port Blox"))
                        player->SetPosition(Vector3(-23708.0, -3363.0, 2259.0));

                    if (ImGui::MenuItem("Crap Chute"))
                        player->SetPosition(
                            Vector3(-42509.0, -35899.0, 1174.0));

                    if (ImGui::MenuItem("Gold Farm"))
                        player->SetPosition(Vector3(21300.0, 41190.0, 2229.0));

                    if (ImGui::MenuItem("Lost Cave (spawn)"))
                        player->SetPosition(
                            Vector3(-54139.0, -56955.0, 1124.0));

                    if (ImGui::MenuItem("Book of Fire"))
                        player->SetPosition(Vector3(-43659.0, -56343.0, 267.0));

                    ImGui::EndMenu();
                }
                ImGui::EndMenuBar();
            }
            ImGui::Text("Position");
            auto const position = player->GetPosition();
            ImGui::Text("x: %f", position.x);
            ImGui::Text("y: %f", position.y);
            ImGui::Text("z: %f", position.z);
            ImGui::InputFloat3("teleport", &gui_state->a.x);
            ImGui::End();
        }

        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }
}