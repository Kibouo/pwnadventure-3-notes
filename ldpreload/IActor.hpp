#pragma once

#include <stddef.h>
#include <cstdint>
#include "DamageType.hpp"

struct Vector3;
struct Rotation;
class IPlayer;
class IUE4Actor;
class IItem;

class IActor {
  public:
    virtual ~IActor(void);
    virtual void * GetUE4Actor(void);
    virtual bool IsNPC(void);
    virtual bool IsPlayer(void);
    virtual IPlayer * GetPlayerInterface(void);
    virtual void AddRef(void);
    virtual void Release(void);
    virtual void OnSpawnActor(IUE4Actor *);
    virtual void OnDestroyActor(void);
    virtual const char * GetBlueprintName(void);
    virtual bool IsCharacter(void);
    virtual bool CanBeDamaged(IActor *);
    virtual int32_t GetHealth(void);
    virtual int32_t GetMaxHealth(void);
    virtual void Damage(IActor *, IItem *, int32_t, DamageType);
    virtual void Tick(float);
    virtual bool CanUse(IPlayer *);
    virtual void OnUse(IPlayer *);
    virtual void OnHit(IActor *, const Vector3 &, const Vector3 &);
    virtual void OnAIMoveComplete(void);
    virtual const char * GetDisplayName(void);
    virtual bool IsElite(void);
    virtual bool IsPvPEnabled(void);
    virtual IItem ** GetShopItems(size_t &);
    virtual void FreeShopItems(IItem **);
    virtual int32_t GetBuyPriceForItem(IItem *);
    virtual int32_t GetSellPriceForItem(IItem *);
    virtual Vector3 GetLookPosition(void);
    virtual Rotation GetLookRotation(void);
    virtual IActor * GetOwner(void);
};
