#pragma once

#include "QuestState.hpp"

class ItemQuestState : public QuestState {
  private:
    class IItem *m_item;
    std::string m_nextState;

  public:
    ItemQuestState(class Quest *, const std::string &, const std::string &, class IItem *, const std::string &);
    virtual void CheckForEarlyCompletion(class IPlayer *);
    virtual void OnItemAcquired(class IPlayer *, class IItem *);
};
