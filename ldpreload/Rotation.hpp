#pragma once

struct Rotation {
    float pitch;
    float yaw;
    float roll;
  public:
    Rotation(void);
    Rotation(float, float, float);
};