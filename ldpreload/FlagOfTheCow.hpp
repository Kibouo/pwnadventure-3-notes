#pragma once

#include "Flag.hpp"

class FlagOfTheCow : public Flag {
  public:
    virtual const char * GetName();
    virtual const char * GetDisplayName();
};