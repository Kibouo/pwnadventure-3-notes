#pragma once

#include "Item.hpp"

class Grenade : public Item {
  public:
    virtual const char * GetCustomCostDescription();
    virtual bool CanEquip();
    virtual void PerformActivate(class IPlayer *);
    virtual bool ShowEventOnPickup();
    virtual void SpawnProjectile(class Player *);
};