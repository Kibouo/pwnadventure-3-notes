#pragma once

#include <stddef.h>
#include <cstdint>

struct QuestStateInfo {
    std::string state;
    uint32_t count;
};