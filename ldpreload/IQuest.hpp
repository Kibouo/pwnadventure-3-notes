#pragma once

class IQuest {
  public:
    virtual const char * GetName();
    virtual const char * GetDescription();
    virtual class IQuestState * GetStartingState();
    virtual class IQuestState * GetStateByName(const char *);
};