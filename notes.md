# Getting started
- Download
- Quick test play
- Check downloaded files
- De-compile libGameLogic.so & main binary (the prior has a more interesting name and info at 1st glance)
  - libGameLogic.so does not seem to be stripped, which is the biggest "mistake" the devs made. Tho, it does happen IRL. And even without leaks, all data can be reversed, it just takes very *very* long...
- Internet search for possible ways to exploit games (try not to spoil).

2 things can be changed about programs:
  - memory at runtime
  - instructions before loading (write permissions on running code is usually disabled, as such it must be edited before being loaded).

# Tweaking memory at runtime
2 important things here:
- base address: the start address of the loaded binary. This is different on every run due to ASLR.
- memory layout of data structures: memory layout helps in knowing what you're looking at, as well as how to get something using offsets. Data structures can be reconstructed with Ghidra.

![](md_img/2021-02-18-20-48-00.png)

Get base address of loaded lib.

![](md_img/2021-02-20-18-13-58.png)
![](md_img/2021-02-20-18-14-50.png)

Set base address in Ghidra such that addresses represent current processes' addresses. Or set it to 0 and Ghidra will report offsets so you can do the calcs yourself.

`gdb` can be used to explore programs at runtime.
![](md_img/2021-02-20-18-24-17.png)

Using static global var names.

![](md_img/2021-02-20-18-31-19.png)
![](md_img/2021-02-20-19-34-37.png)

Or addresses (base address + offset)

![](md_img/2021-02-20-19-40-54.png)
![](md_img/2021-02-20-19-47-16.png)

Or breaking and reading set register values. Break in the `Player::CanJump` fn here. Then read the `Player*` using hints given by Ghidra (hint = `Player* RDI:8 (auto) this`).

![](md_img/2021-02-20-19-49-22.png)
![](md_img/2021-02-20-19-48-39.png)

This data can now be used to edit memory.

Problem with editing memory with gdb is that it's a 1-time edit. Aka, taking damage would decrease health as usual. We don't have "infinite" health :(

For this we can use Cheat Engine instead. It's a memory viewer & editor. You can use trial & error to find memory addresses based on current values and whether/how they change. Or you can simply paste the address we found with gdb. We can then apply our value edits, but even more important, set them to be applied continuously.

![](md_img/2021-02-18-21-22-28.png)
![](md_img/2021-02-18-21-23-31.png)

Here we edited the jump height of the character. Have a safe trip o/

# Tweaking code
## LD_PRELOAD
We will take advantage of the fact that libGameLogic.so is dynamically loaded.

What does dynamically loaded mean? The game's actual binary calls funtions from libGameLogic.so using the declared functions. However, the actual code/"backend"/definition is unknown for the main binary. It is **loaded dynamically** when the binary is ran.

Using the `LD_PRELOAD` env var we can specify a .so which is loaded literally before every other .so. Thus also before libGameLogic.so. If our .so contains a definition for a function referenced by the main binary, that definition is used. Because our .so is loaded before every other .so, it is even preferred over others. This is essentially a MITM attack.

So what do we need for this to work? Basically only to re-write the definition an existing function from libGameLogic.so which we want, the way we want it. But for that we need function declarations, as well as potentially used data type declarations. These can be extracted using Ghidra or gdb's `ptype` command. We prefer the latter as it's cleaner.

We simply compile our code as a .so: `g++ -I./ ./hack.cpp -o hack.so -shared -fPIC -D_GLIBCXX_USE_CXX11_ABI=0`. Some flag info:
- shared: .so file
- fPIC: Position Independent Code. Jumps are relative instead of absolute, which allows the file to be loaded anywhere in memory. Basically needed for libraries as you don't know where they're be loaded in a program's address space.
- D_GLIBCXX_USE_CXX11_ABI: to uniquely identify functions, their declarations are mangled/encoded. This is useful for example such that our main binary can find the correct function in a .so. Different data types, compilers, and version differences can result in different mangled names. This was the case as our local compiler & game's compiler version differed. The `std::string` and `std::__cxx11::basic_string` differ in mangled names. This flag makes sure the correct string type version is used.

### Re-writing the function's definition
![](md_img/2021-02-19-00-43-18.png)

We tweaked the tutorial area's mob spawner such that the re-spawn timer is set to 0 and max mobs alive is 100. More rats spawn if they'd have space to do so :)

Replace `Player::AddItem`, which is called when game gives our character something to hold in the inv. We hardcoded the function such that it gives the item 10000x... as well as all CTF flags and a few legendary items. These extra items are constructed with their respective constructors, and not taken from the static global list of items.
![](md_img/2021-02-19-21-37-29.png)

Picking up tutorial fire ball results in many items and coupled achievements.
![](md_img/2021-02-19-22-10-05.png)

The achievements.
![](md_img/2021-02-19-21-42-23.png)

Picking up another random item gives everything again. The inventory looks messed up but everything in hotkey bar is usable.

### Hooking functions
What if we simply want to introduce side-effects while keeping the original function/logic intact? For this we can use function hooking.

The idea is to hijack a function of choice, tell it to execute our custom code, and then end it by running, and optionally return, the original function.

To access the original function we use `dlsym`. This obtains the address of a requested function in a dynamically loaded library. Example code which hooks on the `Player::CanJump` function:
```
bool Player::CanJump(){
    std::cout << "Player::CanJump got called. Passing on to original fn" << std::endl;

    bool can_jump = false;
    auto orig_lib = dlopen("./libGameLogic.so", RTLD_LAZY);
    if (orig_lib){
        bool (*orig_fn)(Player*) = (bool(*)(Player*))dlsym(orig_lib, "_ZN6Player7CanJumpEv");
        can_jump = (*orig_fn)(this);
        dlclose(orig_lib);
    }

    return can_jump;
}
```

`dlsym` expects a handle to the library, which can be acquired with `dlopen`. The desired function is a class method, but the `dl...` functions are C functions. At first getting the method by name seems complicated. However, methods are converted to "loose" functions which simply take a pointer to the class they operate on. In this case our function would thus become `bool CanJump(Player* this)`. To get this function by name we pass it's mangled name to `dlsym`.

![](md_img/2021-02-22-20-12-15.png)

Here we see our logging as well as the player jumping. Our re-written code set `can_jump` to `false` by default. The fact that we're jumping proves that the original function was called correctly.

Of course this is very basic and only a POC. The possibilities of hooking a function are endless.

#### GUI

We might have overdone it with the `LD_PRELOAD` trick and made a GUI to enable/disable cheats on-the-fly. We used `ImGui` to render the GUI.

![](md_img/2021-03-03-13-36-46.png)

### Finding different function
Earlier, we only got a few achievements from gathering those items. We want all tho!
![](md_img/2021-02-20-20-55-32.png)

We had a look at the de-compiled `Player::MarkAsAchieved` code. It's kinda ugly due to iterators, but readable. Problem is that it accesses static global variables (`g_achievements`) which, if not explicitly marked with the `extern` keyword, are not accessible from the outside.
![](md_img/2021-02-20-21-05-46.png)

This can be confirmed by checking the default (no flag) and dynamic linking (`-D` flag) symbol tables.

As `Player::MarkAsAchieved` itself cannot be replaced satisfactorily, we hardcoded `Player::MarkAsAchieved` calls in the `Player::AddItem` function.
![](md_img/2021-02-19_23-38-07.gif)

Double achievements are no problem. However, getting all achievements causes problems. Having all achievements rewards the player with a flag. This goes wrong for some reason and the game segfaults (we didn't investigate further).

Afterwards we found that accessing static global variables is actually possible. The pre-loaded .so and the original .so which contains the static global variables are part of the same process and thus share the same virtual memory space after all. One caveat is that the variables aren't accessible by name.

To access the variables, we need to get the base address of libGameLogic.so at runtime and access the variables by offset (taken from e.g. Ghidra). To get the base address we use `dl_iterate_phdr` to enumerate all dynamically loaded .so's by the process.

### Byte patching
Next we wanted to open Fort Blox. This location consists of a few logic puzzles.
![](md_img/2021-02-20-21-14-32.png)

Some are simple.
![](md_img/2021-02-20-21-15-21.png)

While others might prove to be a bit more difficult.

Surely this might not be a problem. Just find the function which handles the circuit logic and let it always report that the puzzle is solved, allowing for the door to be opened. We tweak `Player::GetCircuitOutputs` such that it reports every circuit to be open (`true`).

![](md_img/blocky_solved_puzzles.gif)

That was easy, right? Or not...

We cannot interact with the chest! Is it a fake chest? It shouldn't be. Turns out the chest class has a method (`BlockyChest::CanUse`) which checks whether the player is allowed to interact with it. Sure, let's hardcode it to be allowed at all times.

![](md_img/2021-02-20-21-29-44.png)

We can interact with it! Or not...

Pressing E doesn't seem to do anything. After looking around we found the `BlockyChest::PerformUse` function. This likely is what hands the player his items. Another problem pops up however.

![](md_img/2021-02-20-21-31-38.png)

Ghidra fails to de-compile the code. We'll have to take a look at the assembly.

The assembly is quite long which makes it daunting. However, due to appropriate variables/functions being marked with their labels, we can make out the general structure and thus working of this function.

First, the `IsAuthority` function is called. This is for checking authority if you're playing online. This isn't relevant to us.
Then, `GetPosition` and `Distance` are called. The chest thus checks whether the player is close enough.
After that, `GetQuestByName` is called as shown below.
Lastly, several `GetItemByName` calls occur. This is likely where the game hands the player its items.

![](md_img/2021-02-20-21-38-38.png)

Only `GetQuestByName` is confusing. What would a chest's interact function do with a quest? We see it perform some operation on the quest and then branch depending on the result (the `JNZ` line). It seems to jump to the giving of items when not zero/false.

Thus, we deduct it to be a check as to whether the quest is finished. If it is finished (not "not finished") we get out items.

Now that we know why interacting doesn't do anything. Due to hard-coding the doors being opened, we never completed the quest. We continued to look for ways to auto-complete the quest. However, due to some logic errors the game kept crashing whenever we advanced a quest to the next stage.

Thus, we decided to simply patch the code. Instead of checking whether the quest is finished, we want it to give us items when the quest is not finished. Aka we reverse the check.

To do this we simply patch the `JNZ` opcode to become a `JZ` opcode. This is done by replacing the 0x85 byte with 0x84. Practically we did this with Ghidra and re-exported the patched binary (https://github.com/schlafwandler/ghidra_SavePatch).

![](md_img/2021-02-20-21-50-58.png)

The chest opened!
![](md_img/2021-02-20-21-52-07.png)

It had a gold rarity shotgun in it!!!
![](md_img/2021-02-20-22-12-41.png)

What a letdown compared to our hacked fireball...
